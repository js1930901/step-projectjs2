'use strict'

const DATA = [
  {
    'first name': 'Олексій',
    'last name': 'Петров',
    photo: './img/trainers/trainer-m1.jpg',
    specialization: 'Басейн',
    category: 'майстер',
    experience: '8 років',
    description:
      'Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.',
  },
  {
    'first name': 'Марина',
    'last name': 'Іванова',
    photo: './img/trainers/trainer-f1.png',
    specialization: 'Тренажерний зал',
    category: 'спеціаліст',
    experience: '2 роки',
    description:
      "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
  },
  {
    'first name': 'Ігор',
    'last name': 'Сидоренко',
    photo: './img/trainers/trainer-m2.jpg',
    specialization: 'Дитячий клуб',
    category: 'інструктор',
    experience: '1 рік',
    description:
      'Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.',
  },
  {
    'first name': 'Тетяна',
    'last name': 'Мороз',
    photo: './img/trainers/trainer-f2.jpg',
    specialization: 'Бійцівський клуб',
    category: 'майстер',
    experience: '10 років',
    description:
      'Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.',
  },
  {
    'first name': 'Сергій',
    'last name': 'Коваленко',
    photo: './img/trainers/trainer-m3.jpg',
    specialization: 'Тренажерний зал',
    category: 'інструктор',
    experience: '1 рік',
    description:
      'Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.',
  },
  {
    'first name': 'Олена',
    'last name': 'Лисенко',
    photo: './img/trainers/trainer-f3.jpg',
    specialization: 'Басейн',
    category: 'спеціаліст',
    experience: '4 роки',
    description:
      'Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.',
  },
  {
    'first name': 'Андрій',
    'last name': 'Волков',
    photo: './img/trainers/trainer-m4.jpg',
    specialization: 'Бійцівський клуб',
    category: 'інструктор',
    experience: '1 рік',
    description:
      'Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.',
  },
  {
    'first name': 'Наталія',
    'last name': 'Романенко',
    photo: './img/trainers/trainer-f4.jpg',
    specialization: 'Дитячий клуб',
    category: 'спеціаліст',
    experience: '3 роки',
    description:
      'Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.',
  },
  {
    'first name': 'Віталій',
    'last name': 'Козлов',
    photo: './img/trainers/trainer-m5.jpg',
    specialization: 'Тренажерний зал',
    category: 'майстер',
    experience: '10 років',
    description:
      'Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.',
  },
  {
    'first name': 'Юлія',
    'last name': 'Кравченко',
    photo: './img/trainers/trainer-f5.jpg',
    specialization: 'Басейн',
    category: 'спеціаліст',
    experience: '4 роки',
    description:
      'Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.',
  },
  {
    'first name': 'Олег',
    'last name': 'Мельник',
    photo: './img/trainers/trainer-m6.jpg',
    specialization: 'Бійцівський клуб',
    category: 'майстер',
    experience: '12 років',
    description:
      'Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.',
  },
  {
    'first name': 'Лідія',
    'last name': 'Попова',
    photo: './img/trainers/trainer-f6.jpg',
    specialization: 'Дитячий клуб',
    category: 'інструктор',
    experience: '1 рік',
    description:
      'Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.',
  },
  {
    'first name': 'Роман',
    'last name': 'Семенов',
    photo: './img/trainers/trainer-m7.jpg',
    specialization: 'Тренажерний зал',
    category: 'спеціаліст',
    experience: '2 роки',
    description:
      'Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.',
  },
  {
    'first name': 'Анастасія',
    'last name': 'Гончарова',
    photo: './img/trainers/trainer-f7.jpg',
    specialization: 'Басейн',
    category: 'інструктор',
    experience: '1 рік',
    description:
      "Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
  },
  {
    'first name': 'Валентин',
    'last name': 'Ткаченко',
    photo: './img/trainers/trainer-m8.jpg',
    specialization: 'Бійцівський клуб',
    category: 'спеціаліст',
    experience: '2 роки',
    description:
      'Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.',
  },
  {
    'first name': 'Лариса',
    'last name': 'Петренко',
    photo: './img/trainers/trainer-f8.jpg',
    specialization: 'Дитячий клуб',
    category: 'майстер',
    experience: '7 років',
    description:
      'Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.',
  },
  {
    'first name': 'Олексій',
    'last name': 'Петров',
    photo: './img/trainers/trainer-m9.jpg',
    specialization: 'Басейн',
    category: 'майстер',
    experience: '11 років',
    description:
      'Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.',
  },
  {
    'first name': 'Марина',
    'last name': 'Іванова',
    photo: './img/trainers/trainer-f9.jpg',
    specialization: 'Тренажерний зал',
    category: 'спеціаліст',
    experience: '2 роки',
    description:
      "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
  },
  {
    'first name': 'Ігор',
    'last name': 'Сидоренко',
    photo: './img/trainers/trainer-m10.jpg',
    specialization: 'Дитячий клуб',
    category: 'інструктор',
    experience: '1 рік',
    description:
      'Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.',
  },
  {
    'first name': 'Наталія',
    'last name': 'Бондаренко',
    photo: './img/trainers/trainer-f10.jpg',
    specialization: 'Бійцівський клуб',
    category: 'майстер',
    experience: '8 років',
    description:
      'Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.',
  },
  {
    'first name': 'Андрій',
    'last name': 'Семенов',
    photo: './img/trainers/trainer-m11.jpg',
    specialization: 'Тренажерний зал',
    category: 'інструктор',
    experience: '1 рік',
    description:
      'Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.',
  },
  {
    'first name': 'Софія',
    'last name': 'Мельник',
    photo: './img/trainers/trainer-f11.jpg',
    specialization: 'Басейн',
    category: 'спеціаліст',
    experience: '6 років',
    description:
      'Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.',
  },
  {
    'first name': 'Дмитро',
    'last name': 'Ковальчук',
    photo: './img/trainers/trainer-m12.png',
    specialization: 'Дитячий клуб',
    category: 'майстер',
    experience: '10 років',
    description:
      'Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.',
  },
  {
    'first name': 'Олена',
    'last name': 'Ткаченко',
    photo: './img/trainers/trainer-f12.jpg',
    specialization: 'Бійцівський клуб',
    category: 'спеціаліст',
    experience: '5 років',
    description:
      'Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.',
  },
]

const trainersCards = document.querySelector('.trainers-cards__container')
const templateTrainer = document.querySelector('#trainer-card')
const modalTemplate = document.querySelector('#modal-template')
let fragment = document.createDocumentFragment()

function creatTrenerseItem(el) {
  fragment = templateTrainer.content.cloneNode(true)
  const trenersItem = fragment.querySelector('li')
  let name = fragment.querySelector('.trainer__name')
  let img = fragment.querySelector('.trainer__img')

  name.textContent = `${el['last name']} ${el['first name']}`
  img.src = el.photo
  trenersItem.setAttribute('data-experience', `${el.experience}`)
  trainersCards.append(fragment)
}

function creatTreners(array) {
  array.forEach((el) => {
    creatTrenerseItem(el)
  })
}

creatTreners(DATA)

const sortTreners = document.querySelector('.sorting')
sortTreners.removeAttribute('hidden')
const sidebar = document.querySelector('.sidebar')
sidebar.removeAttribute('hidden')

//Модальне відно

trainersCards.addEventListener('click', openModaWindow)

// функція виклику модального вікна

function openModaWindow(element) {
  const trainersClone = modalTemplate.content.cloneNode(true)
  const trenersItem = trainersClone.querySelector('.modal')
  let modalImg = trenersItem.querySelector('.modal__img')
  let modalName = trenersItem.querySelector('.modal__name')
  let modalCategory = trenersItem.querySelector('.modal__point--category')
  let modalExperience = trenersItem.querySelector('.modal__point--experience')
  let modalSpecialization = trenersItem.querySelector(
    '.modal__point--specialization'
  )
  let modalText = trenersItem.querySelector('.modal__text')

  let trainersPhoto = element.target.parentElement
    .querySelector('img')
    .getAttribute('src')

  if (element.target.closest('.trainer__show-more')) {
    document.body.style = 'overflow: hidden'

    DATA.find((el) => {
      if (el.photo === trainersPhoto) {
        modalImg.src = el.photo
        modalName.textContent = `${el['last name']} ${el['first name']}`
        modalCategory.textContent = `Категорія: ${el.category}`
        modalExperience.textContent = `Досвід: ${el.experience}`
        modalSpecialization.textContent = `Напрям тренера: ${el.specialization}`
        modalText.textContent = el.description
      }
    })
    trainersCards.before(trenersItem)

    const btnModal = trenersItem.querySelector('.modal__close')
    btnModal.addEventListener('click', () => {
      closeModaWindow(trenersItem)
    })

    // Закриття модального вікна
  }
}

// функция закриття модального

function closeModaWindow(modalItem) {
  document.body.style = 'overflow: auto'
  modalItem.remove()
}

//сортування
const trainerCards = [...document.querySelectorAll('.trainer')]

const sortButton = document.querySelectorAll('.sorting__btn')
const trainerCardsCopy = trainerCards.map((value) => value)
let sortList = ''

sortTreners.addEventListener('click', sortTrener)

// функція сортування
function sortTrener(element) {
  if (element.target.closest('.sorting__btn')) {
    sortButton.forEach((el) => {
      el.classList.remove('sorting__btn--active')
    })
    element.target.classList.add('sorting__btn--active')
    sortList = element.target.textContent.trim().toLowerCase()
    localStorage.setItem('сортування', sortList)

    sortTrenerList()
  }
}

function sortTrenerList() {
  switch (sortList) {
    case 'за прізвищем':
      trainerCards.sort((a, b) => {
        let aTrener = a.querySelector('.trainer__name').textContent
        let bTrener = b.querySelector('.trainer__name').textContent

        let firstNameA = aTrener.split(' ')[1]
        let firstNameB = bTrener.split(' ')[1]

        let lastNameA = aTrener.split(' ')[0]
        let lastNameB = bTrener.split(' ')[0]

        if (lastNameA === lastNameB) {
          return firstNameA.localeCompare(firstNameB)
        } else {
          return lastNameA.localeCompare(lastNameB)
        }
      })

      fragment.append(...trainerCards)
      trainersCards.append(fragment)

      break

    case 'за досвідом':
      trainerCards.sort(function (a, b) {
        return (
          +b.dataset.experience.slice(0, 2) - +a.dataset.experience.slice(0, 2)
        )
      })
      fragment.append(...trainerCards)
      trainersCards.append(fragment)

      break
    default:
      fragment.append(...trainerCardsCopy)
      trainersCards.append(fragment)

      break
  }
}

// Фильтрування

const filtForm = document.querySelector('form')
const input = [...document.querySelectorAll('[type="radio"]')]
filtForm.addEventListener('submit', filterTreners)

// функція фільтрування
function filterTreners(el) {
  showPreloader()
  setInterval(() => {
    hidePreloader()
  }, 2000)

  el.preventDefault()

  let checkedArr = []

  input.filter((el) => {
    if (el.checked) {
      let item = el.labels[0].textContent.trim().toLowerCase()
      checkedArr.push(item)
    }
  })

  let directionTrener = checkedArr[0]
  let categoryTrener = checkedArr[1]

  localStorage.setItem('напрям', directionTrener)
  localStorage.setItem('категория', categoryTrener)

  let filterArr = DATA.filter((el) => {
    if (
      (categoryTrener === 'всі' || categoryTrener === el.category) &&
      (directionTrener === 'всі' ||
        directionTrener === el.specialization.toLowerCase())
    ) {
      return true
    }
  })

  trainerCards.forEach((el) => {
    el.style = 'display: none'
  })

  trainerCards.filter((elArr) => {
    let name = elArr.querySelector('img').getAttribute('src')
    filterArr.find((el) => {
      if (name === el.photo) elArr.style = 'display: flex'
    })
  })
}

// перезавантаження

window.addEventListener('load', (element) => {
  filterTreners(element)
  hidePreloader()
})

input.forEach((el) => {
  let item = el.labels[0].textContent.trim().toLowerCase()
  if (
    item === localStorage.getItem('напрям') ||
    item === localStorage.getItem('категория')
  ) {
    el.setAttribute('checked', true)
  }
})

if (!localStorage.getItem('сортування')) {
  localStorage.setItem('сортування', 'за замовчанням')
}

sortList = localStorage.getItem('сортування')

sortButton.forEach((el) => {
  if (sortList === el.textContent.trim().toLowerCase()) {
    el.classList.add('sorting__btn--active')
  }
})
sortTrenerList(sortList)

// Показати прелоадер
function showPreloader() {
  document.querySelector('.preloader').style.display = 'block'
}

// Приховати прелоадер
function hidePreloader() {
  document.querySelector('.preloader').style.display = 'none'
}
